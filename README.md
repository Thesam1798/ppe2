<h1>PPE2</h1>
<p>Rééditer par Alexandre Debris.</p>

<h3>License : </h3>
<p>licensed under a <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/deed.en_US">Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International License</a></p>

<h3>Information : </h3>
<p align="center">
        <a href="http://btssio17.com/"><img src="https://img.shields.io/badge/Owner%20:-Lycee%20Merleauponty-brightgreen.svg?style=flat-square" alt="Owner: Lycee Merleauponty"></a>
        <a><img src="https://img.shields.io/badge/Version%20Name%20:-Init%20Done-blue.svg?style=flat-square" alt="Version Name: Init Done"></a>
        <a><img src="https://img.shields.io/badge/Version%20:-1.1-yellow.svg?style=flat-square" alt="Version: 1.1"></a>
</p>

<a href="https://docs.google.com/document/d/e/2PACX-1vQAAx8BoXFK2-9awm7xdi9TOIbss2jjppiXPdVP6K-SkMKuM1WJWp-1BTfDvm8cmoqsZ7R81pmisheZ/pub?embedded=true">
<img src="https://img.shields.io/badge/Log%20Update%20:-Click%20Here-red.svg?style=flat-square" alt="Log Update : Click Here">
</a>