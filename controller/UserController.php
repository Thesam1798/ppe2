<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of userController
 *
 * @author travail
 */
class UserController extends Controller
{

//put your code here
    /**
     * @var User
     */
    private $modUser = null;
    /**
     * @var Commentaire
     */
    private $modCommentaire = null;

    public function login()
    {
        $d['message'] = "Entrez votre login et votre mot de passe";
        if (isset($_POST['singlebutton'])) {
            if (is_null($this->modUser)) {
                $this->modUser = $this->loadModel('User');
            }

            $donnees = array();
//on n'envoie pas à la méthode update que les boutons où autres
            foreach ($_POST as $k => $v) {
                if ($k != 'singlebutton') {
                    if ($k == 'u_password') {
                        $donnees[$k] = sha1($v);
                    } else {
                        $donnees[$k] = $v;
                    }
                }
            }


            $d['user'] = $this->modUser->findFirst(array(
                'conditions' => $donnees));


            if (empty($d['user'])) {
                $d['message'] = 'Login et/ou mot de passe  incorrects';
                Session::set('login', null);
            } else {

                if ($d['user']->u_password_reset === '1') {
                    Session::set('code', $d['user']->u_code);
                    Session::set('role', $d['user']->u_role);
                    $this->redirect('/user/reset');
                } else {
                    $d['message'] = 'Bonjour '.$d['user']->u_prenom.' '.$d['user']->u_nom;
                    Session::set('code', $d['user']->u_code);
                    Session::set('role', $d['user']->u_role);
                    Session::set('login', $d['user']->u_login);
                    $this->redirect('/ent/liste');
                }

            }
        }
        $this->set($d);
    }


    /**
     * resete password utilisateur
     *
     * @param $id
     */
    public function reset($id)
    {

        if (is_null($this->modUser)) {
            $this->modUser = $this->loadModel('User');
        }

        //demande ou excution ?
        if (isset($id)) {

            $cond = [
                'u_code' => $id
            ];

            $user = $this->modUser->findFirst(array(
                'conditions' => $cond));

            $password = sha1($user->u_nom.$user->u_prenom);

            $this->modUser->update([
                'cle' => $cond,
                'donnees' => [
                    'u_password' => $password,
                    'u_password_reset' => 1
                ]
            ]);

            $d['etu'] = $this->modUser->find(array(
                'conditions' => array('u_role' => 'etu')
            ));

            $this->redirect('/user/liste');

        } else {
            $d['message'] = "Merci de redéfinir votre mot de passe.";

            if (isset($_POST['singlebutton'])) {

                $donnees = array();
                foreach ($_POST as $k => $v) {
                    if ($k != 'singlebutton') {
                        if (($k == 'u_password') or ($k == 'u_password_confirm')) {
                            $donnees[$k] = sha1($v);
                        } else {
                            $donnees[$k] = $v;
                        }
                    }
                }

                if ($donnees['u_password'] !== $donnees['u_password_confirm']) {
                    $this->redirect('/user/reset');
                } else {

                    $cond = [
                        'u_code' => Session::get('code'),
                        'u_role' => Session::get('role')
                    ];


                    $this->modUser->update([
                        'cle' => $cond,
                        'donnees' => [
                            'u_password' => $donnees['u_password'],
                            'u_password_reset' => 0
                        ]
                    ]);

                    $d['user'] = $this->modUser->findFirst(array(
                        'conditions' => $cond));

                    $d['message'] = 'Bonjour '.$d['user']->u_prenom.' '.$d['user']->u_nom;
                    Session::set('code', $d['user']->u_code);
                    Session::set('login', $d['user']->u_login);
                    Session::set('role', $d['user']->u_role);
                    $this->redirect('/ent/liste');
                }
            }
        }

        $this->set($d);
    }

    /**
     *Liste des utilisatuer
     */
    public function liste()
    {
        if (Session::get('role') === 'admin') {
            if (is_null($this->modUser)) {
                $this->modUser = $this->loadModel('User');
            }

            $d['etu'] = $this->modUser->find(['conditions' => 1]);

            $this->set($d);
        }else{
            $this->redirect('/');
        }
    }

    /**
     * detaille de l'utilisateur
     *
     * @param $id
     */
    public function detail($id)
    {

        if (is_null($this->modUser)) {
            $this->modUser = $this->loadModel('User');
        }

        if (is_null($this->modCommentaire)) {
            $this->modCommentaire = $this->loadModel('Commentaire');
        }

        $d['user'] = $this->modUser->findFirst(array(
            'conditions' => array('u_code' => $id)
        ));

        if ($d['user'] === false){
            $this->redirect('/user/liste');
        }else{
            $d['commentaires'] = $this->modCommentaire->find(array(
                'conditions' => array('c_login' => $d['user']->u_login)
            ));

            $this->set($d);
        }

    }


    /**
     *Ajoute des user a partire d'un fichier
     */
    public function add()
    {

        if (!(isset($d))) {
            $d['message'] = '';
        }

        //If File upload
        if (isset($_FILES['fileToUpload'])) {

            $file = $_FILES['fileToUpload'];


            //Upload ok ?
            if ($file['error'] > 0) {
                //Dans le cas d'une erreur
                $d['message'] = 'Erreur lors du transfert';
                $this->redirect('/user/add');
            } else {

                //deplacement du fichier
                $extension_upload = strtolower(substr(strrchr($file['name'], '.'), 1));
                $pos = dirname(__DIR__).DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR.sha1($file['name']);
                $nom = $pos.'.'.$extension_upload;
                move_uploaded_file($file['tmp_name'], $nom);

                //verif move
                if (file_exists($nom) === true) {
                    $file = fopen($nom, 'r');

                    $count = 0;

                    //Read line file
                    while ($row = fgets($file)) {

                        //explode line
                        list($nom, $prenom) = explode(" ", $row);

                        $nom = strtolower($nom);
                        $prenom = strtolower($prenom);

                        //Add in array information
                        $user[$count] = [
                            'u_login' => substr($nom, 0,6).substr($prenom,0,1),
                            'u_password' => $_POST['u_promo'],
                            'u_nom' => $nom,
                            'u_prenom' => $prenom,
                            'u_promo' => $_POST['u_promo'],
                            'u_role' => 'etu',
                            'u_password_reset' => 1
                        ];
                        $count++;
                    }

                    //Close file
                    fclose($file);

                    if (is_null($this->modUser)) {
                        $this->modUser = $this->loadModel('User');
                    }

                    $colonnes = ['u_login','u_password','u_nom','u_prenom','u_promo','u_role','u_password_reset'];

                    //Insert in DB all array $user
                    foreach ($user as $item){

                        $temp = $this->modUser->findFirst(array(
                            'conditions' => array('u_login' => $item['u_login'])
                        ));

                        if (!(isset($temp->u_code))){

                            $value = [$item['u_login'],sha1($item['u_password']),$item['u_nom'],$item['u_prenom'],$item['u_promo'],
                                $item['u_role'],$item['u_password_reset']];
                            $this->modUser->insertAI($colonnes,$value);

                        }

                    }

                    $this->redirect('/user/liste');
                }else{
                    //Dans le cas d'une erreur
                    $d['message'] = 'Erreur lors du deplacement du fuchier dans le dossier';
                    $this->redirect('/user/add');
                }
            }
        }
        $this->set($d);
    }


    /**
     * supprime un user
     *
     * @param $id
     */
    public function supr($id){
        if (is_null($this->modUser) and Session::get('role') == 'admin') {
            $this->modUser = $this->loadModel('User');
        }

        $this->modUser->delete([
            'conditions' => ['u_code' => $id]
        ]);

        $this->redirect('/user/liste');

        $this->set($d);
    }


    /**
     *dump des session
     */
    public function logout()
    {
        Session::set('login', null);
        Session::set('code', null);
        Session::set('role', null);
        $this->redirect('/user/login');
    }

}
