<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 07/02/18
 * Time: 16:27
 */

class ComController extends Controller
{
    private $Commentaires;

    /**
     *liste des com
     */
    function liste()
    {

        if (is_null($this->Commentaires)) {
            $this->Commentaires = $this->loadModel('Commentaire');
        }


        $d['commentaires'] = $this->Commentaires->find(array('conditions' => 1));
        if (empty($d['commentaires'])) {
            $this->e404('Page Vide');
        }


        $this->set($d);
    }


    /**
     * supprime un com
     *
     * @param int $id
     */
    function supprimer($id)
    {

        if (Session::get('role') === 'admin')
        {
            if (is_null($this->Commentaires)) {
                $this->Commentaires = $this->loadModel('Commentaire');
            }
            $this->name->delete(array(
                'conditions' => array('c_code' => $id)
            ));

            $d['entreprises'] = $this->Commentaires->find(array(
                    'conditions' => 1)
            );

            if (empty($d['entreprises'])) {
                $this->e404('Page introuvable');
            }

            $this->set($d);
        }else{
            $this->e404('Page introuvable');
        }


    }


    /**
     * system de rendue
     *
     * @param $view
     * @return bool|void
     */
    function render($view)
    {
        if ($view === 'supprimer' || $view === 'modifier') {
            $this->liste();
            parent:: render('liste');
        } else {
            parent:: render($view);
        }
    }

}