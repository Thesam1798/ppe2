<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 08/02/18
 * Time: 10:14
 */
?>


<section class="col-sm-15 table_responsive">

    <table class="table table-bordered table-condensed table-striped" id="liste_ent">
        <thead>
            <tr>
                <th>Nom</th>
                <th class="hidden-xs">Date et Heure</th>
                <th>Type</th>
                <th>Texte</th>
                <th>Entreprise</th>
                <th class="<?= $role ?>">Utilitaire</th>
            </tr>
        </thead>
        <?php foreach ($commentaires as $e): ?>
            <tr>
                <td><?= $e->c_login ?></td>
                <td><?= $e->c_dateheure ?></td>
                <td><?= $e->c_type ?></td>
                <td><?= $e->c_texte ?></td>
                <td style="text-align: center"><a href="<?php echo BASE_URL . '/ent/detail/' . $e->e_code; ?>"  class="btn btn-xs btn-warning" title="Entreprisse"><span class="glyphicon glyphicon-eye-open"></span></a></td>
                <td style="text-align: center" class="<?= $role ?>"><a href="<?php echo BASE_URL . '/user/detail/' . $e->u_code; ?>" class="btn btn-xs btn-warning" title="Etudiant"><span class="glyphicon glyphicon-user"></span></a>   <a href="<?php echo BASE_URL . '/com/supprimer/' . $e->c_code; ?>"  class="btn btn-xs btn-danger" title="Supprimer"><span class="glyphicon glyphicon-remove"></span></a></td>
            </tr>

        <?php endforeach; ?>
    </table>
</section>


