<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 08/02/18
 * Time: 10:14
 */

$title_for_layout = 'Connexion'; ?>

<form class="form-horizontal" method="post" action="<?= BASE_URL ?>/user/reset">
    <fieldset>

        <!-- Form Name -->
        <legend>Modification du mot de passe</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="u_password">Mot de passe</label>
            <div class="col-md-4">
                <input id="u_password" name="u_password" placeholder="Mot de passe" title="Mot de passe"
                       class="form-control input-md" type="password">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="u_password">Confirmation</label>
            <div class="col-md-4">
                <input id="u_password_confirm" name="u_password_confirm" placeholder="Mot de passe" title="Mot de passe"
                       class="form-control input-md" type="password">

            </div>
        </div>
        <!-- Button -->
        <div class="form-group">
            <label class="col-md-2 control-label" for="singlebutton"></label>
            <div class="col-md-4">
                <button id="singlebutton" name="singlebutton" class="btn btn-info">Connexion</button>
            </div>
        </div>
    </fieldset>
</form>
<legend><?= $message ?></legend>
