<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 08/02/18
 * Time: 10:14
 */
?>
<section class="col-sm-15 table_responsive">

    <table class="table table-bordered table-condensed table-striped" id="liste_ent">
        <thead>
            <tr>
                <th>Login</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Promotion</th>
                <th class="<?= $role ?>">Role</th>
                <th>Password Reset</th>
                <th class="<?= $role ?>">Reset</th>
                <th class="<?= $role ?>">Supprimer</th>
                <th class="<?= $role ?>">Detail</th>
            </tr>
        </thead>
        <?php foreach ($etu as $e): ?>
            <tr>
                <td><?= $e->u_login ?></td>
                <td><?= $e->u_nom ?></td>
                <td><?= $e->u_prenom ?></td>
                <td><?= $e->u_promo ?></td>
                <td class="<?= $role ?>"><?= $e->u_role ?></td>
                <td><?php if ($e->u_password_reset == '1'){echo '<button class="btn btn-xs btn-warning">Demander</button>';}else{echo '<button class="btn btn-xs btn-success">Exécuter</button>';} ?></td>
                <td class="<?= $role ?>" style="text-align: center"><a href="<?php echo BASE_URL . '/user/reset/' . $e->u_code; ?>"  class="btn btn-xs btn-danger" title="Password_Reset"><span class="glyphicon glyphicon-refresh"></span></a></td>
                <td class="<?= $role ?>" style="text-align: center"><a href="<?php echo BASE_URL . '/user/supr/' . $e->u_code; ?>"  class="btn btn-xs btn-danger" title="Supprimer"><span class="glyphicon glyphicon-remove"></span></a></td>
                <td class="<?= $role ?>" style="text-align: center"><a href="<?php echo BASE_URL . '/user/detail/' . $e->u_code; ?>"  class="btn btn-xs btn-success" title="Detail"><span class="glyphicon glyphicon-user"></span></a></td>
            </tr>

        <?php endforeach; ?>
    </table>
</section>
