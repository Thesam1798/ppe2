<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 08/02/18
 * Time: 11:18
 */
?>

<form class="form-horizontal" method="post" action="<?= BASE_URL ?>/user/add" enctype="multipart/form-data">
    <fieldset>

        <!-- Form Name -->
        <legend>Etudiant</legend>

        <div class="form-group">
            <label class="col-md-2 control-label" for="fileToUpload">Fichier</label>
            <div class="col-md-4">
                <input type="file" name="fileToUpload" id="fileToUpload" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="u_promo">Promotion</label>
            <div class="col-md-4">
                <input id="u_promo" name="u_promo" class="form-control input-md" type="number" required min="2000" max="9999">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="singlebutton"></label>
            <div class="col-md-4">
                <button class="btn btn-info">Envoyer</button>
            </div>
        </div>


    </fieldset>
</form>
<legend><?= $message ?></legend>