<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 08/02/18
 * Time: 10:14
 */

$title_for_layout = $user->u_nom . " " .$user->u_prenom ?>
<form class="form-horizontal" method="post" action="<?= BASE_URL ?>/user/reset/<?= $user->u_code?>">
    <fieldset>

        <!-- Form Name -->
        <legend>Etudiant</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="u_nom">Nom</label>
            <div class="col-md-4">
                <input id="u_nom" name="u_nom" placeholder="Nom de l'entreprise" class="form-control input-md " disabled="disabled" type="text" value="<?= $user->u_nom; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="u_prenom">Prenom</label>
            <div class="col-md-5">
                <input id="u_prenom" name="u_prenom" placeholder="Adresse principale" title="Adresse  principale"  class="form-control input-md " disabled="disabled" type="text" value="<?= $user->u_prenom; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="u_promo">Promotion</label>
            <div class="col-md-4">
                <input id="u_promo" name="u_promo" placeholder="Complément d'adresse" class="form-control input-md" disabled="disabled" type="text" value="<?= $user->u_promo; ?>">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="u_role">Role</label>
            <div class="col-md-4">
                <input id="u_role" name="u_role" placeholder="Role" class="form-control input-md" disabled="disabled" type="text" value="<?= $user->u_role; ?>">

            </div>
        </div>
    </fieldset>
</form>


<!-- les commentaires -->
<table class="table table-bordered table-condensed table-striped" id="liste_com">
    <thead>
    <tr>
        <th>Auteur</th>
        <th>Date</th>
        <th>Commentaire</th>
        <th>Type</th>
        <th>Entreprise</th>
        <th class="<?= $role ?>">Supprimer</th>
    </tr>
    </thead>
    <?php foreach ($commentaires as $c): ?>
        <tr>

            <td><?= $c->c_login ?></td>
            <td><?= $c->c_dateheure ?></td>
            <td><?= $c->c_texte ?></td>
            <td><?= $c->c_type ?></td>
            <td><a href="<?php echo BASE_URL . '/ent/detail/' . $c->e_code; ?>"  class="btn btn-xs btn-warning" title="Entreprisse"><span class="glyphicon glyphicon-eye-open"></span></a></td>
            <td class="<?= $role ?>"><a href="<?php echo BASE_URL . '/com/supprimer/' . $c->c_code; ?>"  class="btn btn-xs btn-danger" title="Supprimer"><span class="glyphicon glyphicon-remove"></span></a></td>

        </tr>

    <?php endforeach; ?>
</table>